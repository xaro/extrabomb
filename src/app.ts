import { NotificationsService } from './notifications.service';
import * as jquery from 'jquery';

import { VideosPage } from './videos-page.model';

// The postRender endpoint gives additional information like saved times.
const POST_RENDER_URL = 'http://www.giantbomb.com/postRender';

let savedTimes: any = null;

let currentVideoPage: VideosPage;

const NOTIFICATIONS_SERVICE = new NotificationsService();

const VIDEO_BLOCK = jquery('#video-block');

let showingWatchedVideos : boolean = null;

async function initialize(): Promise<void> {
  addHideWatchedVideosButtons();

  // The observer is used to know when a page change happens
  const videoBlockObserver = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
      // Only trigger when the hide class is removed
      if (mutation.attributeName === 'class'
        && !VIDEO_BLOCK.hasClass('hide')) {
        processVideoPage();
      }
    });
  });
  videoBlockObserver.observe(VIDEO_BLOCK.get(0),
    { 'attributes': true, 'attributeFilter': ['class'] });

  processVideoPage();
}

async function addHideWatchedVideosButtons(): Promise<void> {
  const hideWatchedVideosButton = jquery('<a></a>', {
    'click': () => {
      showingWatchedVideos = false;
      currentVideoPage.hideWatchedVideos();
      hideWatchedVideosButton.hide();
      showWatchedVideosButton.show();
    }
  })
    .hide()
    .html(`${iconHtml('eye-close')} Hide watched videos`)
    .prependTo('.sub-nav ul li.active');
  const showWatchedVideosButton = jquery('<a></a>', {
    'click': () => {
      showingWatchedVideos = true;
      currentVideoPage.showWatchedVideos();
      hideWatchedVideosButton.show();
      showWatchedVideosButton.hide();
    }
  })
    .hide()
    .html(`${iconHtml('eye-open')} Show watched videos`)
    .prependTo('.sub-nav ul li.active');

  await loadOptions();
  if (showingWatchedVideos) {
    hideWatchedVideosButton.show();
  } else {
    showWatchedVideosButton.show();
  }
}

async function processVideoPage(): Promise<void> {
  const localSavedTimes = await loadSavedTimes();
  currentVideoPage =
    VideosPage.parseCurrentVideoPage(NOTIFICATIONS_SERVICE, localSavedTimes);

  if (!showingWatchedVideos) {
    currentVideoPage.hideWatchedVideos();
  }
}

// TODO: Extract options to a service.
function loadOptions(): Promise<void> {
  chrome.storage.sync.get(['hideWatchedVideosByDefault'], (items) => {
    if (items['hideWatchedVideosByDefault'] !== undefined) {
      showingWatchedVideos = !items['hideWatchedVideosByDefault'];
    }
  });
  return Promise.resolve();
}

// TODO: Extract savedTimes to a service.
function loadSavedTimes(): Promise<any> {
  if (savedTimes === null) {
    return Promise.resolve(
      jquery.post(POST_RENDER_URL)
        .then((response) => {
          savedTimes = response.data.VideoBundle.times;
          return savedTimes;
        }));
  } else {
    return savedTimes;
  }
}

/// Returns the HTML of an icon as a string.
function iconHtml(iconName: string): string {
  return `<i class="icon icon-${iconName}"></i>`;
}

initialize();