import * as jquery from 'jquery';

// Milliseconds after which the notification will dissapear.
const HIDE_AFTER_MILLIS = 2500;

export class NotificationsService {
  private messageBlockElement : JQuery;

  constructor() {
    this.messageBlockElement = jquery('#message-block');
  }

  /**
   * Shows a notification that appears on top of the page and dissapears
   * after a while.
   */
  public showNotification(text: string): void {
    this.messageBlockElement.text(text);
    this.messageBlockElement.show();

    setTimeout(() => this.messageBlockElement.hide(), HIDE_AFTER_MILLIS);
  }
}