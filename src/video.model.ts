import * as jquery from 'jquery';

import { NotificationsService } from './notifications.service';

// CSS class for video elements that are already watched.
const WATCHED_CLASS = 'watched';

// Seconds that can be left unwatched in a video to be considered
// as watched.
const THRESHOLD_PERCENTAGE_FOR_WATCHED_VIDEOS = 0.95;

const SAVE_TIME_URL = 'http://www.giantbomb.com/api/video/save-time/';

export class Video {
  private watchedTimeSeconds: number;
  private markAsWatchedButton = jquery('<button>', {
    'text': 'Mark as watched',
    'class': 'mark-as-watched'
  });

  constructor(
    readonly notificationsService: NotificationsService,
    readonly id: string,
    readonly lengthSeconds: number,
    watchedTimeSeconds: number,
    readonly element: JQuery) {
    this.watchedTimeSeconds = watchedTimeSeconds;

    if (this.isWatched()) {
      this.markAsWatched();
    } else {
      this.addMarkAsWatchedButton();
    }
  };

  /// Removes the video from the dom.
  public remove(): void {
    this.element.detach();
  }

  /// Returns true if the video has less time left than a specific threshold.
  public isWatched(): boolean {
    return (this.watchedTimeSeconds / this.lengthSeconds)
      > THRESHOLD_PERCENTAGE_FOR_WATCHED_VIDEOS;
  }

  private markAsWatched(): void {
    this.watchedTimeSeconds = this.lengthSeconds;
    this.element.addClass(WATCHED_CLASS);
    this.markAsWatchedButton.remove();
  }

  private addMarkAsWatchedButton(): void {
    this.element.append(() => {
      return this.markAsWatchedButton
        .on('click', this.saveVideoWatchTime.bind(this));
    });
  }

  private saveVideoWatchTime(): void {
    jquery.post(SAVE_TIME_URL,
      { 'video_id': this.id, 'time_to_save': this.lengthSeconds })
      .then((response) => {
        if (response.success === 1) {
          this.markAsWatched();
          this.notificationsService.showNotification(
            'Video marked as watched.');
        } else {
          // TODO: Show proper error color
          this.notificationsService.showNotification(
            'Failed to mark video as watched.');
        }
      });
  }
}