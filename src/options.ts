const HIDE_WATCHED_VIDEOS_BY_DEFAULT_ID = 'hide-watched-videos';

function saveOptions() {
  const hideWatchedVideosByDefaultInput = getHideWatchedVideosByDefaultInput();

  chrome.storage.sync.set({
    'hideWatchedVideosByDefault': hideWatchedVideosByDefaultInput.checked
  }, () => {
    // Update status to let user know options were saved.
    const status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function () {
      status.textContent = '';
    }, 750);
  });
}

function restoreOptions() {
  const hideWatchedVideosByDefaultInput = getHideWatchedVideosByDefaultInput();

  chrome.storage.sync.get({
    'hideWatchedVideosByDefault': false
  }, (items) => {
    hideWatchedVideosByDefaultInput.checked =
      items['hideWatchedVideosByDefault'];
  });
}

function getHideWatchedVideosByDefaultInput(): HTMLInputElement {
  return document
    .getElementById(HIDE_WATCHED_VIDEOS_BY_DEFAULT_ID) as HTMLInputElement;
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('save').addEventListener('click',
  saveOptions);