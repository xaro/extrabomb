import * as jquery from 'jquery';

import { Video } from './video.model';
import { NotificationsService } from './notifications.service';

export class VideosPage {
  videos: Video[];

  static parseCurrentVideoPage(notificationsService: NotificationsService,
    savedTimes: Object): VideosPage {
    const currentVideoPage = new VideosPage(notificationsService);
    currentVideoPage.videos = [];

    jquery('#video-block ul.editorial > li').each((index, element) => {
      const videoElement = jquery(element);
      const id = videoElement.find('.video-progress').data('video-id');
      const length = Number.parseFloat(
        videoElement.find('.video-progress').data('video-length'));
      currentVideoPage.videos.push(
        new Video(
          notificationsService, id, length, savedTimes[id], videoElement));
    });

    return currentVideoPage;
  }

  constructor(readonly notificationsService: NotificationsService) { };

  public hideWatchedVideos(): void {
    this.videos
      .filter((video) => video.isWatched())
      .forEach((video) => video.remove());
  }

  public showWatchedVideos(): void {
    this.videos
      .filter((video) => !video.isWatched())
      .forEach((video) => video.remove());
    this.videos
      .forEach((video) => video.element.appendTo('#video-block ul.editorial'));
  }
}