# ExtraBomb

Google Chrome extension that adds extra features to GiantBomb.com.

# TODO
* Fix the issue when trying to go forward or backwards in a video with the
keyboard arrows after clicking on the play/pause button.
* Add playlists support.